export class Budget {
    title: string = '';
    amount: number = 0;
    description: string = '';
}
