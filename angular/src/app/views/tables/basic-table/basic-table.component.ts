import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.scss']
})
export class BasicTableComponent implements OnInit, OnDestroy {

  tableName: string = "";
  rows: any[] = [];
  columns: any[] = [];
  sub: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route
        .data
        .subscribe(
            data => {
              this.tableName = data.name

            }
        );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
