<?php
/**
 * Created by PhpStorm.
 * User: stefa
 * Date: 13/02/2019
 * Time: 11:03
 */

namespace App\Controller;

use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * Class ArticleController
 * @package App\Controller
 *
 * @Route("/api")
 */
class ArticleController extends AbstractController
{
    /**
     * Lists all Articles
     * @FOSRest\Get("/articles")
     *
     * @return JsonResponse
     */
    public function getArticles()
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);

        // query for all Articles
        $articles = $repository->findAll();

        return new JsonResponse($articles, Response::HTTP_OK);
    }

    /**
     * Create Article
     * @FOSRest\Post("/article")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createArticle(Request $request)
    {
        $article = new Article();
        $article->setName($request->get('name'));
        $article->setDescription($request->get('description'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        return new JsonResponse($article, Response::HTTP_CREATED);
    }
}